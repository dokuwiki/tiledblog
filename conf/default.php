<?php
/**
 * Options for the Blog Plugin
 */
$conf['formPosition']	= 'top';	// position of new entry form
$conf['iconSize']	= 100;		// the icon size
$conf['adminGroup']	= 'admin';	// who can clear the cache
$conf['sampleDelai']	= 2*60*60;	// delai to change sample
?>
