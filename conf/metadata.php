<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Metadata for configuration manager plugin
 * Additions for the tiledBlog plugin
 */
$meta['formPosition']	= array('multichoice',
				'_choices' => array ('top', 'bottom', 'none'));
$meta['iconSize']	= array('numeric');
$meta['adminGroup']	= array('string');
$meta['sampleDelai']	= array('numeric');
?>
