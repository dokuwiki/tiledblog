/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * Javascript functionality for the tiledblog plugin
 */

// ========================================
// Ajax function
// ========================================
/* performe ajax request */
function tiledblogAjax (action, ns) {
    jQuery.ajax ({
	type: "POST",
	url:  DOKU_BASE+"lib/plugins/tiledblog/ajax.php",
	cache: false,
	async: true,
	data: "tiledblog[action]="+action+"&tiledblog[ns]="+ns,
    });
}

// ========================================
