<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */

// commands
$lang['clear']		= 'Clear cache';
$lang['clearAll']	= 'Clear all cache';
