<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */
 
// for the configuration tiledblog plugin
$lang['formPosition']	= 'position of the new entry form';
$lang['iconSize']	= 'tile icon size';
$lang['adminGroup']	= 'who can clear the cache';
$lang['sampleDelai']	= 'cache time delay for sample (sec)';
?>
