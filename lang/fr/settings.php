<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * French language file
 */

// for the configuration tiledblog plugin
$lang['formPosition']	= 'position du formulaire du nouveau billet';
$lang['iconSize']	= 'taille de l\'icon de tuile';
$lang['adminGroup']	= 'qui peut effacer le cache';
$lang['sampleDelai']	= 'temps de cache pour l\'exemple (en secondes)';
?>
