<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * French language file
 */

// commands
$lang['clear']		= 'Effacer le cache';
$lang['clearAll']	= 'Effacer tout les caches';
