<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * TiledBlog Plugin: manage clear action action
*/
if (!defined ('DOKU_INC'))
  define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
require_once (DOKU_INC.'inc/init.php');
require_once (DOKU_INC.'inc/common.php');
require_once (DOKU_INC.'inc/auth.php');
{
  global $INFO;
  if (isset ($_SERVER['REMOTE_USER']))
    $INFO['userinfo'] = $auth->getUserData ($auth->cleanUser ($_SERVER['REMOTE_USER']));
  $tiledblogPug =& plugin_load ('syntax', 'tiledblog');
  if (!$tiledblogPug->isAdmin ())
    die ();

  global $conf;
  $savedir = ((!$conf['savedir'] || strpos ($conf['savedir'], '.') === 0) ? DOKU_INC : "").$conf['savedir']."/";
  $cacheDir = $savedir."cache/tiledblog/";
  switch ($_REQUEST ['tiledblog']['action']) {
  case 'clear':
    $baseName = $cacheDir.md5 ($_REQUEST ['tiledblog']['ns']);
    @unlink ($baseName."-tile.cache");
    @unlink ($baseName."-sample.cache");
    break;

  case 'clearAll':
    $exclude = ".|..";
    $exclude_array = explode("|", $exclude);
    $pathDir = rtrim ($cacheDir, "/") . "/";
    if (!is_dir($pathDir))
      break;
    $pathDirObj = opendir ($pathDir);
    while (false !== ($file = readdir ($pathDirObj))) {
      if (in_array (strtolower ($file), $exclude_array))
	continue;
      $pathFile = $pathDir.$file;
      if (!is_file ($pathFile))
	continue;
      if (eregi ('.*-tile\.cache$', $file, $b) ||
	  eregi ('.*-sample\.cache$', $file, $b))
	@unlink ($pathFile);
    }
    break;
  }
}
